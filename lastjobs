#! /bin/bash
set -u
readonly PROGNAM=${BASH_SOURCE##*/}
errmsg()
{
    echo "${PROGNAM}: $1" >&2
}

# Dependencies, external commands called by this script.
readonly DATE='/usr/bin/date'
readonly GAWK='/usr/bin/gawk'
readonly GETOPT='/usr/bin/getopt'
readonly SACCT='/usr/bin/sacct'
readonly SINFO='/usr/bin/sinfo'
readonly SORT='/usr/bin/sort'

chkdeps()
{
#
# Check whether required external commands are present and executable, and if so,
# then print the version of bash and the version of each of the required commands.
#
	local deps="${DATE} ${GAWK} ${GETOPT} ${SACCT} ${SINFO} ${SORT}"
	local state=0
	local cmd
	for cmd in ${deps}
	do	
		if [ ! -x "${cmd}" ]
		then
			state=1
			errmsg "[E] Required executable '${cmd}' not found or not executable!"
			continue
		fi
	done
	if [ ${state} -ne 0 ]
	then
		return ${state}
	fi
	local -a pipstat
	local version_info
	echo "# ${PROGNAM}, version information of required commands:"
	for cmd in /bin/bash ${deps}
	do
		"${cmd}" --version | while IFS= read -r version_info
		do
			echo "# ${cmd}: ${version_info}"
			break # Always read and print just the 1st line.
		done
		pipstat=("${PIPESTATUS[@]}")
		if [ ${pipstat[0]} -ne 0 ]
		then
			errmsg "[E] '${cmd} --version' command failed!"
			exit 1
		fi
	done
} 
if [ $# -gt 0 ]
then
	if [ "$1" == "--check-dependencies" ]
	then
		if ! chkdeps
		then
			exit 1
		fi
		exit 0
	fi
fi

#
# Sacct query output format specification.
# The attributes can be changed and their order can be changed in the
# definition of FMT, as long as the 'end' attribute name is part of the
# specification, since this is the key to sort on. Attribute names must
# be all lowercase and must not contain a %<length> suffix.
# Sorting and filtering routines interpret FMT and will adapt as needed
# as long as there is an end field and pipe-separated line records
# are used (sacct -P). See function configure_..., sacct_query,
# sacct_sort, and sacct_filter, below.
#
readonly FMT='start,end,user,jobid,jobname,submit,timelimit,elapsed,state,nnodes,nodelist'

# Time window size for sacct query in days
readonly MIN_DAYS=1
readonly DEFAULT_DAYS=3
DAYS=${DEFAULT_DAYS}

#
# The time window is set, but empty, by default. It must NOT be set to
# any other default value. It is to be set to a specific timestamp by a
# command line option, and if that does not happen the empty value is
# replaced by the current time.
# 
TIMEWINDOW_END=''

# Maximum number of jobs
readonly MIN_MAXJOBS=1
readonly DEFAULT_MAXJOBS=8
MAXJOBS=${DEFAULT_MAXJOBS} 

# By default running jobs are 'recently active jobs' and not ignored.
EXCLUDE_RUNNING='no'

VERBOSE='no'

usage()
{
local outtext
if ! read -r -d '' outtext <<EOF
NAME
${PROGNAM} -- List most recently active jobs per specified batch node.

SYNOPSIS
${PROGNAM} -h|--help
${PROGNAM} -V|--version
${PROGNAM} --check-dependencies

${PROGNAM} [ -v|--verbose ] 
  [ --exclude-running ] 
  [ --days <n> ]
  [ --maxjobs <n> ]
  [ --before <YYYY>-<MM>-<DD>T<hh>:<mm>:<ss> ]
  <nodeset> [ nodeset .. ] 

DESCRIPTION
What ${PROGNAM} produces, is an inverted job history of the jobs that are,
or were, active on each specified batch node: the currently running jobs,
if any, come first, followed by the last job that finished on node, followed
by the job last-but-one job that finished, and so on, until at some point the
job history is cut off. The time window that is queried is tunable with
options, and also the number of jobs listed, irrespective of what was
dug up by the query, is tunable.
 
By default, ${PROGNAM}, for each node specified, lists a short history
of up to ${DEFAULT_MAXJOBS} job records that describe the most recent jobs
that each node has been involved with in the time window from ${DEFAULT_DAYS}
days ago, until just 'now', i.e.: thee invocation time of this tool. Nodes are
specified by one or more space-separated nodeset expressions, which can be
simple specific node names, such as 'node1', 'node2', as well as complexer
ranged string, such as 'node[1-10,12,33],host[6,7,42]', or a combination
of both types of specifications. Specificatiosn that are not recognized to
denote a batch node of the cluster are ignored, filtered out.

The job records, produced by the Slurm 'sacct' tool, are line records that
contain pipe-separated values of the following job attributes, in the
order shown:

  ${FMT}

See the sacct(1) manual page for further explanation what each of these
job attribute names denote.

The most recently active jobs on a node are those that are still running
and have their - yet to become known - end time somewhere in the future.
${PROGNAM} therefore presents the job records of currently running
jobs on a node first, subsequently followed by the jobs that have been
terminated, in decreasing order of their job end time. The list of job
records is truncated when the maximum number of jobs for a job history has
been reached.
The time duration spanned by the default time window can be fairly short,
typically less than the maximum job duration, but showing more than just
the currently running job, or just one most recently finished job, is
particularly important for investigating nodes that entered an undesirable
state - draining, failing, down, etc. - while being used in shared mode. Where
more jobs run simultaneously, there also can be more than a single culprit,
and even if there is just a single one, it could be the one that finished
last, or the one that finished just before that, and so on.
Use the '--verbose' option to be informed about how many more jobs were
found to be active in the queried time window, but not shown in the short
job history. Use options '--maxjobs' and/or '--exclude-running' to enlarge,
casu quo to change, what is made visible.
There are also options to change the time window of the query. For nodes
that are in a drain/fail/down state, using the '--before' option with a
timestamp of the reported last state change gives a good chance to get
the process listed that  caused that state change.
Each node's job history is preceded by a header listing the node name
and the current state of the node, obtained by the Slurm sinfo(1) command.
If the node is in a sub-optimal non-productive state, there is a 'reason'
for that that is listed, and a timestamp that says when that reason and
the last state change kicked in. This may provide further clues as to
what jobs active around that time might be involved in causing the
state change. If the node is currently in an active reservation, that
reservation is listed in the header too.

Options
  -h|--help
    Print this built-in help text and exit.
  -V|--version
    Show version information and exit.
  --check-dependencies
    'Installation help' / ´debug function' that checks whether all
    required executables are in place and indeed executable. If so, then
    their version information is listed. If all tests succeed an all
    version information could be obtained, exit with status 0, otherwise
    exit with status 1.
  -v|--verbose
    Shows what the values of a number of variables passed on to
    implementation commands such as sacct(1) are, after processing of
    command line options. This option also shows a more elaborate header
    for the job history of each node, detailing statistics about job
    records excluded from the job history.
  --exclude-running
    Ignore running jobs, if any, do not include them in the reported
    job history.
  --days <n>
    Specifies the length of the time window for the job history in days.
    The minimum is ${MIN_DAYS}, the default is ${DEFAULT_DAYS}. The
    start of the time window is <n> day(s) before the end of the sacct
    time window, where the end of the time window by default is 'now',
    i.e.: the current time.
  --maxjobs <n>
    Specifies the maximum number of jobs listed per job history. The
    minimum is ${MIN_MAXJOBS}, the default is ${DEFAULT_MAXJOBS}.
  --before <YYYY>-<MM>-<DD>T<hh>:<mm>:<ss>
    Specifies another, earlier, end of the sacct time window than the
    default, 'now'. Jobs that were not active on the node before the
    specified timestamp, are ignored.

BUGS, FEATURES
A warning is issued if '${PROGNAM}' is NOT run with effective UID 0.
Depending on the details of the Slurm configuration, the capability to
retrieve information about jobs may be restricted for non-privileged users
to such an extent that the tool cannot give the complete information
requested.
This command uses Slurm commands sinfo(1) and sacct(1). If the number of
nodes queried by sinfo is large, and if the amount of jobs that sacct has to
retrieve information of, because the time window is large and the number of
nodes is high, this may have a temporary but noticeable impact on the job
dispatching and job completion througput of the Slurm scheduler and database
daemons. This command issues a single sinfo call, for all nodes specified.
Furthermore, this command issues a dedicated sacct call for each node that
was specified.
A re-write, using just a single sacct call and combining it with more parsing
and reordering of the thus obtained information is feasible. Still, in that
single sacct query roughly the same amount of information is gathered as is
gathered now by the sum of node-dedicated sacct calls.

NOTES
The 'lastjobs' tool is a complete rewrite, with a considerable number of
functional extensions and tuning options added, of an original script,
named 'last_known.job.sh', written by Máni Xander (Lenovo).

EOF
then
	:	# Always non-zero because end of file was read.
fi
echo "${outtext}"
}

hint_usage()
{
    errmsg "[I] Use the '-h' or '--help' option to obtain usage information!"
}

version()
{
#
# According to the GNU coding standards, every command line tool should
# support two standard options; --help and --version.
#
local outtext
if ! read -r -d '' outtext <<EOF
lastjobs v. 2.1
Public repository: <https://gitlab.com/hjstoff/lastjobs> 

Copyright (C) 2023 SURF
License GPLv3+: GNU GPL version 3 or later
    <https://gnu.org/licenses.org/gpl.html> (last visited: 220230905)
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
EOF
then
	:	# Always non-zero because end of file was read.
fi
echo "${outtext}"
}

readonly SHORTOPTS='h,v,V'
readonly LONGOPTS='help,version,verbose,exclude-running,days:,maxjobs:,before:'
TEMP=$("${GETOPT}" -o ${SHORTOPTS} --long "${LONGOPTS}" -n "${PROGNAM}"  -- "$@")
if [ $? -ne 0 ]
then
    hint_usage
	exit 1
fi
# N.B.: The quotes around "$TEMP" are essential!
eval set -- "$TEMP"
unset TEMP

valid_natural_number()
{
#
# True if "$1" has a valid format for a whole number larger than 0,
# expressed in terms of decimal digits, false otherwise. Whitespace prefixes
# and suffixes, and  redundant 0 prefixes, are not accepted.
#
    [[ $1 =~ ^[1-9][0-9]*$ ]]
}

valid_slurm_timestamp()
{
#
# True if "$1" has a valid format for a 'Slurm format' timestamp, false otherwise.
# No whitespace prefixes and suffixes are accepted.
#
    [[ $1 =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[[0-9]{2}:[0-9]{2}:[0-9]{2}$ ]]
}

while true
do
	case "$1" in
        '-h'|'--help')
		usage
		exit 0
		;;
	'V'|'--version')
		version
		exit 0
		;;		
	'--days')
            if ! valid_natural_number "$2" 
            then
                errmsg "[E] '--days' option, invalid option argument!"
                exit 1
            fi 
            DAYS="$2"
            shift 2
            ;;
	'--before')
            if ! valid_slurm_timestamp "$2" 
            then
                errmsg "[E] '--before' option, invalid option argument!"
                exit 1
            fi 
            TIMEWINDOW_END="$2"
            shift 2
            ;;
	'--maxjobs')
            if ! valid_natural_number "$2" 
            then
                errmsg "[E] '--maxjobs' option, invalid option argument!"
                exit 1
            fi 
            MAXJOBS="$2"
            shift 2
            ;;
	'--exclude-running')
            EXCLUDE_RUNNING='yes'
            shift 1
            ;;
        '-v'|'--verbose')
            VERBOSE='yes'
            shift 1
            ;;
        '--')
            shift 1
            break
            ;;
        *)
            #
            # Script logic error: If all options specified to getopt are in sync with all
            # define short and long options handled in the above, this should not be possible!
            #
            errmsg "[E] Unexpected option processing error!"
            exit 1
            ;;
	 esac
done
if [ $# -eq 0 ]
then
    errmsg "[E] No node(s) specified!"
    hint_usage
    exit 1
fi
if [ $EUID -ne 0 ]
then
    errmsg "[W] Not running with EUID 0, not all relevant information may be obtained!"
fi

initialize_nodelist()
{
#
# Initialize global associative array NODELIST, with key node name, from the 
# remaining command line arguments. The corresponding value is a bunch of
# current state attributes, to be provided by Slurm's sinfo(1).
# entries, with the node name being the key.
# The nodes denoted in "$1" could contain duplicates, and sinfo will produce
# multiple lines for a nodes included im more than one partition anyhow.
# Putting the nodes as keys in an associative array is idempotent with a sort
# "unique" on nodename.
# It is a bit tricky to specify a format to sinfo that gives a reliable separation
# of node names and the rest of the attributes. Especially the "reason" of why a
# is down/drained/failed can contain pretty much an printable  character - spaces
# and Lenovo has also made hashmarks (#) and pipes(|) part of the reason contents.
# The function implementation deals with this by making a control character - a
# formfeed -  the separator in the format string and then sets IFS to a formfeed
# for the read command, so as to split what is read in a node and an attribute
# part.
#
    local node attrib 
    #
    # More trickyness:
    # It is possible that just garbage node names are supplied, that are all
    # then silenty ignored by sinfo. The consequence would be that the body of 
    # of the wile loop is nevere entered.
    # To make ${#NODELIST[@]} properly report a length 0 in such a case, 
    # the value of an empty array MUST be assigned. Declaring alone (setting the
    # associative array attribute on the NODELIST variable) just is NOT enough.
    # Without assigning a value, the name is still unbound.
    #
    declare -g -A NODELIST=()
    while IFS=$'\f' read -r node attrib
    do
	NODELIST["${node}"]="${attrib}"
    # N.B.: changing the sinfo format has consequences for the sacct_filter routine, parse_nodeattribs, below, which parses the output!
    done < <("${SINFO}" --Node --noheader --format '%N'$'\f''state=<%T>,since=<%H>,by=<%u>,reason=<%E>,reservation=<%i>' --nodes  "$1")
    if [ ${#NODELIST[@]} -eq 0 ]
    then
        errmsg "[E] No specified nodename(s) found by Slurm (sinfo(1))!"
        exit 1
    fi
}

#
# The initialize_nodelist function is called with a concatenation of all arguments
# still left into a single argument. They are supposedly nodenames or nodeset
# expressions. 
# As long as there is just a single option argument presented to the sinfo
# "--nodes" option, sinfo is very tolerant and does not seem to care whether
# the argument supplied contains multiple nodes or nodeset expressions separated
# by blanks, or by commas, or by a mixture of both. It does not even care if there
# are redundant commas or whether there are commas with nothing in between but
# redundant blanks. Any string that does not denote a nodename is - silently,
# without an error or warning - filtered out sinfo. 
#
initialize_nodelist "$*"

configure_timewindow_parameters()
{
    if [ -z "${TIMEWINDOW_END}" ]
    then
        # Set to current time date in format supported by Slurm (without TZ suffix).
        TIMEWINDOW_END=$("${DATE}" '+%FT%T')
        if [ $? -ne 0 -o -z "${TIMEWINDOW_END}" ]
        then
            errmsg "[E] Failed to determine current time!"
            exit 1
        fi
    fi
    # 
    # Get the numeric timezone suffix (that Slurm does not like for input!) for
    # TIMEWINDOW_END, whatever that is in the currently active time zone.
    # Numeric date calculations with GNU date go wrong if the timezone suffix is
    # lacking. Run date with the --debug option with additions or subtraction of
    # days to/from a specified date, to see how the date command interprets the
    # '+' or '-' sign before the mentioned amount of days as the plus or minus
    # sign of the - lacking - numeric timezone!
    # The command is supposed to assert the local timezone whenever that is missing,
    # but apparently cannot handle that with date calculations in conjunction with
    # the timestamp input format used here.
    # For comparable cases, also see:
    # https://unix.stackexchange.com/questions/497639/subtract-time-using-date-and-bash
    # (Last visited: 2023-08-25).
    #
    local timezone_suffix 
    timezone_suffix=$("${DATE}" -d "${TIMEWINDOW_END}" '+%z')
    if [ $? -ne 0 -o -z "${timezone_suffix}" ]
    then
            errmsg "[E] Failed determine implicit timezone suffix for timestamp"
            exit 1
    fi
    TIMEWINDOW_START=$("${DATE}" -d "${TIMEWINDOW_END}${timezone_suffix} - ${DAYS} days" '+%FT%T') 
    if [ $? -ne 0 -o -z "${TIMEWINDOW_START}" ]
    then
        errmsg "[E] Failed determine --starttime option argument for sacct query!"
        exit 1
    fi 
}

configure_endndx()
{
#
# Define the ENDNDX parameter for the sacct_sort and sacct_filter routines.
# This is the field number of the (first) 'end' attribute in the sacct output
# records, and so, the field to sort on, given value of FMT, the sacct output
# format specification string.
# The order of fields and composition of the sacct output specification,
# FMT, can be changed as desired, with the restriction that it must always
# contain 'end' (job end time), which is needed as the sort key for the
# job records.
#
    local -a arr
    local i
    ENDNDX=0
    IFS=',' read -r -a arr <<< "${FMT}"
    for i in "${!arr[@]}"
    do
        if [ "${arr[$i]}" == 'end' ]
        then
            ENDNDX=$((i + 1)) # Bash arrays are indexed offset 0, sort(1) counts fields from offset 1. 
            break
        fi
    done
    if [ ${ENDNDX} -eq 0 ]
    then
        errmsg "[E] No 'end' attribute found in sacct output specification string '${FMT}'!"
        exit 1
    fi
}

list_configuration()
{
local outtext
if ! read -r -d '' outtext <<EOF

# ${PROGNAM}: Configured parameters for sacct query, sacct sort, and sacct_filter.
# - Sacct output record, specification of job attributes and job attribute order:
#   FMT               = '${FMT}'
# - Sacct format for output of timestamp attributes (e.g. 'start' or 'end'):
#   SLURM_TIME_FORMAT = '${SLURM_TIME_FORMAT}' 
# - Sacct query time window parameters:
#   DAYS              = ${DAYS}
#   TIMEWINDOW_START  = '${TIMEWINDOW_START}'
#   TIMEWINDOW_END    = '${TIMEWINDOW_END}'
# - Sacct sort field, index, position,  of 'end' values in query outputi records:
#   ENDNDX            = ${ENDNDX}
# - Sacct filter option to limit the number of job records listed:
#   MAXJOBS           = ${MAXJOBS} 
# - Sacct filter option to exclude job records of running jobs (if any):
#   EXCLUDE_RUNNING   = '${EXCLUDE_RUNNING}'

EOF
then
	:	# Always non-zero because end of file was read.
fi
echo "${outtext}"
}

configure()
{
    configure_timewindow_parameters
    configure_endndx
    #
    # Unfortunately, Slurm timestamps by default include no timezone and the specifying of
    # of unambiguous -S and -E option arguments to sacct, using seconds since the Epoch,
    # is not even supported.
    # In any timezone where a daylight savings time regime applied during part of the year,
    # sorting on Slurm timestamps is buggy twice a year, viz. when the sacct query time
    # window spans an event of switching to, or switching back fromi, applying the daylight
    # savings regime. By choosing a time format with a prefix of constant width, expressing
    # time in seconds since the Epoch, lexicographic sorting of such values will correct
    # this periodic buggyness. With a width of 10 decimal digits, seconds since the Epoch
    # can be expressed until somewhere in October 2286! That will do 'for now'.
    # A constant width is needed, because we want lexicographic sorting rather than
    # numeric sorting of the key, so it can contain any character after the constant width
    # numeric prefix and can be sensibly and predictably be compared to strings NOT
    # starting with a decimal digit. In particular, the timestamp of the 'end' job attribute
    # can also have the non-numeric value 'Unknown' (for jobs still running), which then
    # also behaves exactly as we want with lexicographics comparisons with strings
    # starting with digits.
    #
    export SLURM_TIME_FORMAT='%10s=%FT%T'
    if [ "${VERBOSE}" == 'yes' ]
    then
        list_configuration
    fi
}

sacct_query()
{
# Run sacct query, where "$1" must denote a batch single node.
        "${SACCT}" -N $1 --starttime "${TIMEWINDOW_START}" --endtime "${TIMEWINDOW_END}" \
        --duplicates -XP --noheader --format "${FMT}"
}

sacct_sort()
{
    LANG='C' ${SORT} -t '|' -k ${ENDNDX},${ENDNDX} --reverse
}

sacct_filter()
{
#
# Filter sacct query output for a single node, where "$1" is the node name,
# "$2" is a current node state description produced by sinfo(1).
#
    "${GAWK}" -bF '|' -v PROGNAM="${PROGNAM}" -v node="$1" -v nodeattribs="$2" \
    -v verbose="${VERBOSE}" -v fmt="${FMT}" \
    -v maxjobs="${MAXJOBS}" -v exclude_running="${EXCLUDE_RUNNING}" -v endndx="${ENDNDX}" \
    -v timewindow_start="${TIMEWINDOW_START}" -v timewindow_end="${TIMEWINDOW_END}" '
    function is_running() {
        return $endndx == "Unknown";
    }
    function parse_nodeattribs() {
    # Parse and simplify nodeattribs value.
    # This relies on a format being defined by shell function initialize_nodelist, above.
        # Reservation info is the last attrib. Strip it if reservation info (%i) is empty.
        sub(/,reservation=<>[ \t]*/, "", nodeattribs);
        # If the node is idle or running production, none of this (%H) (%u) (%E0 applies.
        sub(/,since=<Unknown>,by=<Unknown>,reason=<none>/, "", nodeattribs);
    }
    BEGIN {
        delete A;    # Forces A to be an array.
        exclude_count = 0;
        truncate_running_count = 0;
        truncate_finished_count = 0;
        A_ndx = 0;
    }
    # Sacct is run --noheader, ALL lines are line records.
    {
        if (is_running()) {
            if (exclude_running == "yes") {
                ++exclude_count; 
                next;
            }
        }
        if (A_ndx < maxjobs) {
            ++A_ndx;
            A[A_ndx] = $0;
        }
        else {
            if (is_running()) {
                ++truncate_running_count;
            }
            else {
                ++truncate_finished_count;
            }
        }
    }
    END {
        printf("# Node: \047%s\047\n", node);
        parse_nodeattribs();
        printf("# Current state (sinfo): %s\n", nodeattribs);
        if (NR == 0) {
            printf("%s: [E] No jobs found for node \047%s\047 in specified time window \047%s\047 - \047%s\047!\n",
            PROGNAM, node, timewindow_start, timewindow_end) > "/dev/stderr";
            exit(2);
        }
        if (verbose == "yes") {
            printf("# %d job records found in specified time window \047%s\047 - \047%s\047\n",
            NR, timewindow_start, timewindow_end);
            if (exclude_count > 0) {
                printf("# %d job records of jobs, found to be still running, a priori excluded from job history\n",
                exclude_count); 
            }
            if (A_ndx == maxjobs) {
                if (truncate_running_count > 0) { 
                    printf("# %d job records of jobs, found to be still running, excluded from job history because of maxjobs = %d limit.\n",
                    truncate_running_count, maxjobs);
                 
                }
                if (truncate_finished_count > 0) {
                    printf("# %d job records of jobs, that finished within the time window less recently, excluded from job history because of maxjobs = %d limit.\n",
                    truncate_finished_count, maxjobs);
                } 
            }
        }
        printf("# sacct, pipe-separated fields: %s\n", fmt);
        for (i = 1; i <= A_ndx; ++i) {
            print A[i];
        }
        print "";
        exit(0);
    }
    '
}

run_pipeline()
{
#
# Called with $1 = node name, $2 = sinfo status description for the node.
# Run sacct query, sort, and filter pipeline for a single node.
# Return 0 on no error.
# Return 1 on a general failure error advise the caller to stop invoking the 
# the pipeline for new instances.
# Return 2 on error that is presumably node-specific. E.g. nodename not found.
# The caller is advised to continue invoking the pipeline for other node names,
# if any.
#

    local status=0
    local -a pipstat
    sacct_query "$1" | sacct_sort | sacct_filter "$1" "$2"
    pipstat=("${PIPESTATUS[@]}")
    if [ ${pipstat[0]} -ne 0 ]
    then
        errmsg "[E] Failure running sacct query! (exit status = ${pipstat[0]})"
        return 1
    fi
    if [ ${pipstat[1]} -ne 0 ]
    then
        errmsg "[E] Failure running sort on sacct query result! (exit status = ${pipstat[1]})"
        return 1
    fi
    if [ ${pipstat[2]} -ne 0 ]
    then
        # The filter exits with 2 if no errors occurred but no records were found, and has already output a diagnostic for this.
        if [ ${pipstat[2]} -ne 2 ] 
        then
                errmsg "[E] Failure running gawk filter on sorted sacct query result! (exit status = ${pipstat[2]})"
                return 1
        fi
        return 2
    fi
    return 0 
}

main()
{
    if [ ${VERBOSE} == 'yes' ]
    then
	echo "# Processing ${#NODELIST[@]} nodes ..."
    fi
    configure
    local status
    local node
    for node in ${!NODELIST[@]}
    do
        run_pipeline "${node}" "${NODELIST[${node}]}"
        status=$?
        if [ $status -ne 0 -a $status -ne 2 ]
        then
            exit 1
        fi
    done
}

main
